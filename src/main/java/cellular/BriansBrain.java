package cellular;
import java.util.Random;

import datastructure.IGrid;
import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
    }

    public CellState getCellState(int row, int column) {
        CellState status = currentGeneration.get(row, column);
		return status; 
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    private int countNeighbors(int row, int col, CellState state) {
		int neighborCount = 0;
		for (int r = row-1; r <= row+1; r++){
			for (int c = col-1; c <= col+1; c++) {
				if (r >= currentGeneration.numRows() || c >= currentGeneration.numColumns() || r < 0 || c < 0) {
					continue;
				}
				if (r == row && c == col) {
					continue;
				}
				else if (state == currentGeneration.get(r, c)) {
					 
					neighborCount++;
				}

			}
		}
		return neighborCount;
	}


    @Override
    public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int r = 0; r < numberOfRows(); r++){
			for (int c = 0; c < numberOfColumns(); c++) {
				nextGeneration.set(r, c, getNextCell(r, c));
				
				}
			}
		currentGeneration = nextGeneration;
		}


		
    @Override
    public CellState getNextCell(int row, int col) {
        // CELLE LEVER
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        // CELLE DØENDE
        else if (getCellState(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        // CELLE DØD
        else if (getCellState(row, col) == CellState.DEAD){
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
