package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {


    private int cols;
    private int rows;
    private CellState[][] grid;


    /**
     * konstrukør som lager en ny grid og setter alle verdiene i grid til en oppgitt status
     * for loopene kjører gjennom alle mulige indexer i den nye griden mtp. oppgitt rekker og kolonner
     * @param rowsinput
     * @param columns
     * @param initialState
     */
    public CellGrid(int rowsinput, int columns, CellState initialState) {
		rows = rowsinput;
        cols = columns;
        grid = new CellState[rowsinput][columns];
        for (int r = 0; r < rows; r++ ){
            for (int c = 0; c < columns; c++) {
                set(r, c, initialState);
            }
        }   
        
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }
    /**
     * sett plass i grid, ut i fra oppgitt kolonne og rekke, til oppgitt status
     * ikke tillat ugyldige verdier
     */
    @Override
    public void set(int row, int column, CellState element) {
        if (0 <= column & column < numColumns()){
            if (0 <= row & row < numRows()){
                grid[row][column] = element;
            }
            else {
                throw new IndexOutOfBoundsException();
            }
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        
    }

    /**
     * returner statusen til cellen i oppgitt rekke og kolonne
     * ikke tillat ugyldige verdier
     */
    @Override
    public CellState get(int row, int column) {
        CellState contentsOfCell = null;
        if (row >= 0 && row <= numRows()) {
            if (column >= 0 && column <= numColumns()){
                contentsOfCell = grid[row][column];  
            } 
            else {
                throw new IndexOutOfBoundsException();
            }
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        
        return contentsOfCell;  
    }

    /**lag en ny grid gjennom metoden Cellgrid, bruk originale grid sine verdier (row og cols) og sett cellstate lik noe random
     * kjør gjennom alle mulige indexer i originalgrid og sett copiedGrid sin grid lik den originale grid sin cellstate
     * returner den kopierte grid
     */
    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(rows, cols, CellState.ALIVE);
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++){
                copiedGrid.grid[x][y] = this.grid[x][y];
            }
        }
        return copiedGrid;
    }
}

